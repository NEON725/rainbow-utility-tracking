extern crate hyper;
extern crate hyper_rustls;
extern crate google_sheets4 as sheets4;
extern crate yup_oauth2;

use std::env;
use sheets4::{Sheets, api::ValueRange};
use serde_json;

#[derive(serde::Serialize, serde::Deserialize)]
struct Config
{
	spreadsheet_id: String,
	sheet_name: String,
	fields: Vec<String>,
}

#[tokio::main(flavor = "current_thread")]
async fn main()
{
	let config: Config = serde_json::from_reader(std::io::BufReader::new(std::fs::File::open("config.json").expect("Could not open config.json."))).expect("Could not read config.json.");

	let max_range = format!("'{}'!A1:Z9999", config.sheet_name);
	let max_range = max_range.as_str();

	let fields = config.fields;

	let mut args = env::args();
	if args.len() > fields.len() + 1
	{
		panic!("Expected at most {} arguments.", fields.len());
	}

	args.nth(0);
	let insertion_data: Vec<String> = args.collect();

	let secret = yup_oauth2::read_application_secret("cli-credentials.json")
		.await
		.expect("client secret could not be read");
	let auth = yup_oauth2::InstalledFlowAuthenticator::builder(
			secret,
			yup_oauth2::InstalledFlowReturnMethod::Interactive
		)
		.persist_tokens_to_disk("tokencache.json")
		.build()
		.await
		.expect("Failed to build authenticator.");

	let hub = Sheets::new
	(
		hyper::Client::builder().build
		(
			hyper_rustls::HttpsConnectorBuilder::new()
				.with_native_roots()
				.https_or_http()
				.enable_http2()
				.build()
		),
		auth,
	);

	let (_, spreadsheet) = hub
		.spreadsheets()
		.get(&config.spreadsheet_id)
		.include_grid_data(true)
		.doit()
		.await
		.expect("Failed to query spreadsheet.");

	println!("Found spreadsheet: {}", spreadsheet.spreadsheet_url.unwrap());

	let sheet = spreadsheet
		.sheets.as_ref().expect("Could not read sheets.")
		.iter().find(
			|sheet|
				sheet
					.properties.as_ref().expect("Could not read sheet properties.")
					.title.as_ref().expect("Sheet had no title.")
				== &config.sheet_name
		)
		.expect("Could not find main sheet.");
	let sheet_props = sheet.properties.as_ref().unwrap();
	println!("Found sheet: {} - {}", sheet_props.title.as_ref().unwrap(), sheet_props.sheet_id.expect("Sheet had no ID."));

	let data = sheet.data.as_ref().expect("Could not read grid data.").first().expect("Sheet grid data empty.");

	let row_data = data.row_data.as_ref().expect("Row data empty.");
	let first_row_data = row_data.first().expect("First row empty.");
	let first_row_data = first_row_data.values.as_ref().expect("Could not read first row.");
	let first_row_data: Vec<String> = first_row_data.iter().map(|data| data.formatted_value.to_owned().unwrap()).collect();

	let insertion_mapping: Vec<(String, usize)> = first_row_data.iter().map(|field_name| (field_name.to_owned(), fields.iter().position(|f| f == field_name).unwrap_or(usize::MAX))).collect();

	println!("Header: {}", first_row_data.join(","));

	let append_row_data: Vec<Option<String>> = insertion_mapping.iter().map(|(field_name, mapping)|
	{
		(
			field_name,
			match insertion_data.get(mapping.to_owned())
			{
				None => None,
				Some(dat) => Some(dat.to_owned()),
			},
		)
	})
	.map(|(field_name, value)|
	{
		if value.is_some() {return value;}
		match field_name.as_str()
		{
			"Date" => Some(chrono::Local::now().date_naive().format("%-m/%-d/%Y").to_string()),
			_ => None
		}
	}).collect();

	let append_row_data: Vec<String> = append_row_data.iter().map(|o| o.to_owned().unwrap_or("".to_string())).collect();
	println!("Appending: {}", append_row_data.join(","));

	let append_data: Vec<Vec<String>> = vec!(append_row_data);
	hub.spreadsheets().values_append
		(
			ValueRange
			{
				major_dimension: Some("ROWS".to_string()),
				range: Some(max_range.to_string()),
				values: Some(append_data)
			},
			spreadsheet.spreadsheet_id.unwrap().as_str(),
			max_range
		)
		.value_input_option("USER_ENTERED")
		.doit()
		.await
		.expect("Failed to append data.");
}